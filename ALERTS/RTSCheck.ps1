﻿Function SendMail($From, $Body, $To, $Subject, $Attachment ) {
    $SMTPServer = "relay.dynamite.ca"
    $SMTPPort = "25"
    Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -BodyAsHtml
}

Function BoldAndRed($var, $value, $operand) {
    if($operand -eq "greaterThan") {
        if($var -gt $value) {
            return " style=`"font-weight: bold; font-color:red;`""
        }
    }
    if($operand -eq "lessThan") {
        if($var -lt $value) {
            return " style=`"font-weight: bold; font-color:red;`""
        }
    }
    if($operand -eq "notEqual") {
        if($var -ne $value) {
            return " style=`"font-weight: bold; color:red;`""
        }
    }
}

#Variables
$rtlogCount = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' -ErrorAction SilentlyContinue | Foreach-Object {Get-Content $_.FullName | Where-Object {$_ -like '*rtlog_*'}} | Measure-Object
[regex]$regex = 'upseq\\rtlog_00\d{3}'
$resourceFiles = New-Object System.Collections.ArrayList($null)
$rtlog = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' -ErrorAction SilentlyContinue | Foreach-Object {
    $content = Get-Content $_.FullName
    if($content -like '*rtlog*') { 
        $resourceFiles.Add($content) 
    } 
}
$stores = @{}
foreach($r in $resourceFiles) {
    $regex.Matches($r) | foreach-object{
        $value = $_.Value.replace('upseq\rtlog_00','')
        if($stores -contains $value) {
            $count = $stores[$value].ToString().ToInt32()+1
            $stores.Add($value,$count)
        }
        else {
            $stores.Add($value,1)
        }
    }
}
Set-Content C:\users\jnehme\desktop\stores.txt -Value $stores
$eeupdateCount = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' -ErrorAction SilentlyContinue | Foreach-Object {Get-Content $_.FullName | Where-Object {$_ -like '*eeupdate*'}} | Measure-Object
$momCount = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' -ErrorAction SilentlyContinue | Foreach-Object {Get-Content $_.FullName | Where-Object {$_ -like '*mom_*'}} | Measure-Object
$taxCount = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' -ErrorAction SilentlyContinue | Foreach-Object {Get-Content $_.FullName | Where-Object {$_ -like '*tax*'}} | Measure-Object
$queueCount = Get-Childitem -path '\\sapthh05\e$\ajbrts\batches\rec' | Measure-Object
$ftgen = Get-WmiObject -Namespace "root\cimv2" -Class Win32_Process -Impersonation 3 -ComputerName "SAPTHH05" | Select Name | select-String -pattern "ftgen" | measure-object
$ftserv = Get-WmiObject -Namespace "root\cimv2" -Class Win32_Process -Impersonation 3 -ComputerName "SAPTHH05" | Select Name | select-String -pattern "ftserv" | measure-object
$tcpcomn = Get-WmiObject -Namespace "root\cimv2" -Class Win32_Process -Impersonation 3 -ComputerName "SAPTHH05" | Select Name | select-String -pattern "tcpcomn" | measure-object
$rtsexpd2 = Get-WmiObject -Namespace "root\cimv2" -Class Win32_Process -Impersonation 3 -ComputerName "SAPTHH05" | Select Name | select-String -pattern "rtsexpd2" | measure-object
$dataflow = Get-WmiObject -Namespace "root\cimv2" -Class Win32_Process -Impersonation 3 -ComputerName "SAPTHH05" | Select Name | select-String -pattern "DataFlow" | measure-object

if( $rtlogCount.Count -ge 200 -or $queueCount.Count -ge 1000 -or $ftgen.Count -ne 1 -or $ftserv.Count -ne 3 -or $tcpcomn.Count -ne 10 -or $rtsexpd2.Count -ne 1 -or $dataflow.Count -ne 1) {
    $htmlStores = "None"
    if($stores.Keys -gt 1)
    {
        $htmlStores = $stores.GetEnumerator() | ConvertTo-Html -Property Name, Value -Title "Stores with RTLogs waiting"
    }
    $Body = "<p>Hello,</p>
    <p>Please see report below:</p>
    <ul>
    <li $(BoldAndRed -var $queueCount.Count -value 2000 -operand "greaterThan")>Total number of items in the queue: $($($queueCount).Count)</li>
    <li $(BoldAndRed -var $rtlogCount.Count -value 200 -operand "greaterThan")>Number of RTLogs in the queue: $($($rtlogCount).Count)</li>
    <li>Number of EEUpdates in the queue: $($($eeupdateCount).Count)</li>
    <li $(BoldAndRed -var $momCount.Count -value 0 -operand "notEqual")>Number of MOM in the queue: $($($momCount).Count)</li>
    <li>Number of Tax in the queue: $($($taxCount).Count)</li>
    <li>Services Report:
    <ul>
    <li $(BoldAndRed -var $ftgen.Count -value 1 -operand "notEqual")>FTGEN: $($($ftgen).Count) of 1 online</li>
    <li $(BoldAndRed -var $ftserv.Count -value 3 -operand "notEqual")>FTSERV: $($($ftserv).Count) of 3 online</li>
    <li $(BoldAndRed -var $dataflow.Count -value 1 -operand "notEqual")>DATAFLOW: $($($dataflow).Count) of 1 online</li>
    <li $(BoldAndRed -var $tcpcomn.Count -value 10 -operand "notEqual")>TCPCOMN: $($($tcpcomn).Count) of 10 online</li>
    <li $(BoldAndRed -var $rtsexpd2.Count -value 1 -operand "notEqual")>RTSEXPD2: $($($rtsexpd2).Count) of 1 online</li>
    </ul>
    </li>
    </ul>
    <table style=`"border: 1px black; width: 100%; height: 100%; margin-left: auto; margin-right: auto;`" border=`"solid`">
    <tbody>
    <tr>
    <th>List of stores with RTLogs</th>
    </tr>
    <tr>
    <td style=`"text-align: center;`">$htmlStores</td>
    </tr>
    </tbody>
    </table>
    <p>Thanks</p>"

    SendMail -From "RTS Alert <RTSAlert@dynamite.ca>" -Body $Body -To "<jnehme@dynamite.ca>" -Subject "Warning! Please check RTS"
} 