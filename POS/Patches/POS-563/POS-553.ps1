## Tokenization enablement on POS and BO middleware

## Steps are to turn off services, replace files, turn on services (or reboot)

## Start by finding the services to be shutdown
$javaProcess = Get-Process -Name 'java.exe'
$beasvcProcess = Get-Process -Name 'beasvc.exe'

$weblogicService = Get-Service | Where-Object { $_.Name -like 'beasvc *' -or $_.Name -like '*WeblogicDomain*' }
$StoreService = Get-Service -Name 'StoreServer'
$fipayService = Get-Service -Name 'fipayeps'

try {
    if($weblogicService -is $null){
        $javaProcess.Kill()
    }
    else {
        $StoreService.Stop()
        $weblogicService.Stop()
    }

    $fipayService.Stop()
} 
catch {
    Write-Output 'Something went wrong when stopping the services'
}

## path of the files needed
$rootPath = ".\Files"
$rootPathCan = "CANADA"
$rootPathCommon = "COMMON"
$rootPathUS = "US"
$rootPathPOS = "POS"
$rootPathBO = "BO"
$rootPathSQL = "SQL"

## Common locations
$patchesFolder = "patches"

## Server locations
$serverRetailStore = "C:\OracleRetailStore\Server\pos"
$serverRTLog = "config\rtlog"

## POS locations
$fipayLocation = "C:\fipayeps"
$posRetailStore = "C:\OracleRetailStore\Client\pos"
$libLocalesFolder = "lib\locales"

function copyFile($path, $dest){
    Get-ChildItem "$($path)" | foreach-object { Copy-Item $_.FullName -Destination "$($dest)\$($_.Name)" -Force }
}

function copyStoreFile($brand) {
    $fileLocation = "$($rootPath)\$($rootPathPOS)"
    $destLocation = "$($posRetailStore)\$($libLocalesFolder)"

    switch($env:STORE_LANGUAGE) {
        "EN" {Copy-Item "$($fileLocation)\en_$($brand).jar" -Destination "$($destLocation)\en.jar"}
        "FR" {Copy-Item "$($fileLocation)\fr_$($brand).jar" -Destination "$($destLocation)\fr.jar"}
    }
}

## START PROCESS
# BO
if($env:STORE_CASH -eq '000') {
    ## Deploy patch .GDHF-05-POS-531.jar and .GDHF-04-POS-531.jar into C:\OracleRetailStore\Client and Server\pos\patches
    copyFile("$($rootPath)\$($rootPathCommon)", "$($serverRetailStore)\$($serverRTLog)")

    ## Overwite the existing RTLog Mapping
    copyFile("$($rootPath)\$($rootPathBO)", "$($serverRetailStore)\$($patchesFolder)")

    ## Password expiry fix
    start-process cmd -argumentlist "/c sqlplus system/N3tsys11 @$($rootPath)\$($rootPathSQL)\passwords.sql"
}
## POS
else {
    ## Deploy patch .GDHF-05-POS-531.jar and .GDHF-04-POS-531.jar into C:\OracleRetailStore\Client and Server\pos\patches
    copyFile("$rootPath\$rootPathCommon", "$($posRetailStore)\$($patchesFolder)")
    if($env:STORE_COUNTRY -eq 'CA') {
        ## Copy fipay file
        copyFile("$($rootPath)\$($rootPathCan)", "$($fipayLocation)")
    }
    else {
        ## Copy fipay file
        copyFile("$($rootPath)\$($rootPathCan)", "$($fipayLocation)")
    }

    ## Copy library locales
    switch($env:STORE_BANNER) {
        "COMBO" {copyStoreFile("CMB")}
        "DYNAMITE" {copyStoreFile("DYN")}
        "GARAGE" {copyStoreFile("GAR")}
    }
}

## restart POS and BO
$weblogicService = Get-Service | Where-Object { $_.Name -like 'beasvc *' -or $_.Name -like '*WeblogicDomain*' }
$StoreService = Get-Service -Name 'StoreServer'
$fipayService = Get-Service -Name 'fipayeps'