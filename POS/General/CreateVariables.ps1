#Create Variables

$banner = ""
$locale = ""
$language = ""
$country = ""
$StoreID = ""
$StoreCash = ""

FUNCTION CheckAndCreate($varName, $value) {
    start-process cmd -ArgumentList "/c setx -m $($varName) $($value)"
}

checkAndCreate -varName "STORE_CASH" -value ([String]([Environment]::GetEnvironmentVariable("COMPUTERNAME"))).Split("-")[2]

$sys = ([String]([Environment]::GetEnvironmentVariable("COMPUTERNAME"))).Split("-")[1]
checkAndCreate -varName "ENV_SYS" -value $(if($sys -eq 'UAT') { return 'UAT' } elseif($sys -eq 'TST') { 'TST'} else { 'PROD' })

if($env:STORE_CASH -ne '000'){
    $path = "C:\fromhq\posclient\ORPOS-13.4.1\ant.install.properties"
    if(Test-Path -Path $path) {
        $temp = (Select-String -path $path -Pattern "input\.install\.brand\s*=\s*\w+").Line.split("=")[1] -replace "\s",""
        $banner = IF($temp -eq 'GDYN') { 'DYNAMITE' } elseif($temp -like '*GRG*') { 'GARAGE' } else { 'COMBO' }
        $locale = (Select-String -path $path -Pattern "input\.locale\.default\s*=\s*\w+").Line.split("=")[1] -replace "\s",""
        $language = (($locale.split("_")[0]) -replace "\s","").toUpper()
        $country = (($locale.split("_")[1]) -replace "\s","").toUpper()
        $StoreID = ((Select-String -path $path -Pattern "input\.configured\.store\.id\s*=\s*[0-9]+").Line.split("=")[1] -replace "\s","").Substring(2,3)
        $StoreCash = ((Select-String -path $path -Pattern "input\.register\.number\s*=\s*[0-9]+").Line.split("=")[1] -replace "\s","") #This is to validate only. But not necessary
    }
}
else {
    $path = "C:\OracleRetailStore\Server\pos\config\application.properties"
    if(Test-Path -Path $path) {
        $temp = (Select-String -path $path -pattern "Manager\.\w\w\w\wCustomerSurveyReward=com.\w+.orpos.pos.services.printing.\w+CustomerSurveyReward")
        $banner = IF($temp -like '*GDYN*') { 'DYNAMITE' } elseif($temp -like '*GRG*') { 'GARAGE' } else { 'COMBO' }
        $locale = (Select-String -path $path -pattern "locale_Default=\w+").Line.Split("=")[1] -replace "\s",""
        $language = (($locale.split("_")[0]) -replace "\s","").toUpper()
        $country = (($locale.split("_")[1]) -replace "\s","").toUpper()
        $StoreID = ((Select-String -path $path -Pattern "StoreID\s*=\s*[0-9]+").Line.split("=")[1].Substring(2,3)) -replace "\s",""
        $StoreCash = "000" #This is to validate only. But not necessary
    }
}

CheckAndCreate -varName "STORE_BANNER" -value $banner
CheckAndCreate -varName "STORE_COUNTRY" -value $country
CheckAndCreate -varName "STORE_LANGUAGE" -value $language
CheckAndCreate -varName "STORE_NUMBER" -value $StoreID
CheckAndCreate -varName "STORE_CASH" -value $StoreCash