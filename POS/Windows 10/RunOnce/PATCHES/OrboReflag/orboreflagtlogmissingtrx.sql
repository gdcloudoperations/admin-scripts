   set feedback off
   set verify off
   set termout off
   set trimspool on
   set linesize 5000
   set pages 0
   set long 2000000000
   set serveroutput on
   spool c:\sql\ORBOReflagTLogMissingTrx.spool.txt

-- DDL for DB Link to CO from BO DB
DROP DATABASE LINK "CORPL";

--------------------------------------------------------
--  DDL for DB Link CO
--------------------------------------------------------

  CREATE DATABASE LINK "CORPL"
   CONNECT TO "RCO_SCHEMA" IDENTIFIED BY VALUES '0576135357C3919A8B403F9F7F1D9B19C12BD311E0190B4ED3'
   USING '(DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = stoprd-scan.corp.gdglobal.ca)(PORT = 1521))
    )
    (CONNECT_DATA =
          (SERVER=dedicated)
      (SERVICE_NAME = stoprd.corp.gdglobal.ca)
    )
  )
  ';

/
     DECLARE
        L_status_code                VARCHAR2(1000) := NULL;
        L_error_code                 VARCHAR2(1000) := NULL;
        L_error_message              VARCHAR2(1000) := NULL;
        L_program                    VARCHAR2(50) := 'BO_check_missing_transaction.sql';
        L_id_str_rt                  tr_trn.id_str_rt%TYPE;
        L_id_ws                      tr_trn.id_ws%TYPE;
        L_dc_dy_bsn                  tr_trn.dc_dy_bsn%TYPE;
        L_ai_trn                     tr_trn.ai_trn%TYPE;
        L_rowid                      ROWID; --VARCHAR2(20);
        L_store                      as_tl.id_str_rt%TYPE;
        L_prior_days                 NUMBER := 0;
        L_max_repl_attempts          NUMBER := 0;
        L_trx_CO_Count               Number := 0;
        L_trx_BO_Count               Number := 0;
        L_ExecID                     VARCHAR(50) := to_char(SYSDATE, 'YYYYMMDD.HH24MISS');
        L_ReplType                   VARCHAR(10) := 'TLOG';

--      Change the following variable value to increase/restrict how far back the utility should check
        L_XPriorDays                 Number := 45;
        L_LatestBusinessDate         VARCHAR(10);
        L_cpt_trx_read               integer := 0;
        L_cpt_trx_upd                integer := 0;
        L_LogLine                    VARCHAR(1024);
        L_LogTxt                     VARCHAR(1024);
        L_LogMissingTrxFound         VARCHAR2(1) := 'N';


-- Cursor to load StoreID Variable
        CURSOR c_get_store IS
        SELECT DISTINCT id_str_rt
          FROM as_tl;

-- Cursor for latest closed business date
        CURSOR c_get_last_businessdate IS
            select to_char(to_date(max(DC_DY_BSN), 'YYYY-MM-DD') , 'YYYY-MM-DD')
            from tr_ctl
            where TY_TRN_CTL = 7;


-- Cursor to detect impacted dates                   
        CURSOR c_recheck_dates IS
            WITH 
                BO_COUNT AS(
                
                        SELECT  ID_STR_RT, DC_DY_BSN, COUNT(1) BO_TRX_COUNT
                        FROM TR_TRN 
                        WHERE DC_DY_BSN BETWEEN to_char(trunc(sysdate - L_XPriorDays), 'YYYY-MM-DD')  and L_LatestBusinessDate
                        AND ID_STR_RT = L_store
                        GROUP BY ID_STR_RT, DC_DY_BSN
                        
                        ),
                
                CO_COUNT AS(
                
                        SELECT  ID_STR_RT, DC_DY_BSN, COUNT(1) CO_TRX_COUNT
                        FROM TR_TRN@CORPL 
                        WHERE DC_DY_BSN BETWEEN to_char(trunc(sysdate - L_XPriorDays), 'YYYY-MM-DD')  and L_LatestBusinessDate
                        AND ID_STR_RT = L_store
                        GROUP BY ID_STR_RT, DC_DY_BSN
                        
                        )     
            SELECT BO.ID_STR_RT, BO.DC_DY_BSN, BO.BO_TRX_COUNT, CO.CO_TRX_COUNT
            FROM BO_COUNT BO
                JOIN CO_COUNT CO ON BO.DC_DY_BSN = CO.DC_DY_BSN AND BO.ID_STR_RT = CO.ID_STR_RT
            WHERE BO.BO_TRX_COUNT > CO.CO_TRX_COUNT;


-- Cursor to fetch ROWIDs that need to be re-sent to BO --> CO replication
        CURSOR c_get_trx IS
            WITH missing_trx AS (
                    SELECT tt.id_str_rt, tt.id_ws, tt.dc_dy_bsn, tt.ai_trn
                    FROM orbo_owner.tr_trn tt
                    WHERE tt.id_str_rt = L_store
                        AND tt.DC_DY_BSN = L_dc_dy_bsn
                    
                    MINUS 
                    
                    SELECT ctrs.id_str_rt, ctrs.id_ws, ctrs.dc_dy_bsn, ctrs.ai_trn
                    FROM tr_trn@CORPL ctrs
                    WHERE ctrs.id_str_rt = L_store
                        AND ctrs.dc_dy_bsn = L_dc_dy_bsn
--                    AND (ctrs.fl_tlog_ok = 1 OR (ctrs.fl_tlog_ok = 0 AND ctrs.fl_cnt_rtry_tlog >= 3)))
 --                   AND (ctrs.fl_tlog_ok = 1 OR (ctrs.fl_tlog_ok = 0 AND ctrs.fl_cnt_rtry_tlog > 3))
            )
            SELECT th.rowid row_id, th.ai_trn, th.id_ws 
            from TR_TRN TH
            JOIN missing_trx MTX on TH.id_str_rt = MTX.id_str_rt AND TH.id_ws = MTX.id_ws and TH.DC_DY_BSN = MTX.dc_dy_bsn AND TH.ai_trn = MTX.ai_trn;


-- Main 
     BEGIN
        OPEN c_get_store;
        FETCH c_get_store INTO L_store;
        CLOSE c_get_store;
        L_ExecID := L_store||'.'|| L_ExecID;
        L_LogLine := 'Execution ID: ' || L_ExecID || ' Store: '||L_store||' Execution start time : '||to_char(SYSDATE, 'YYYY-MM-DD hh24:mi:ss');
        L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
        dbms_output.put_line (L_LogLine);

        OPEN c_get_last_businessdate;
        FETCH c_get_last_businessdate INTO L_LatestBusinessDate;
        CLOSE c_get_last_businessdate;


        L_LogLine :='Execution ID: ' || L_ExecID || ' Store: '||L_store|| ' Checking Business dates from: '|| to_char(trunc(sysdate - L_XPriorDays), 'YYYY-MM-DD') ||' to: '|| L_LatestBusinessDate;
        L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
        dbms_output.put_line (L_LogLine);



        FOR  c_recheck_dates_rec IN c_recheck_dates
        LOOP
            L_id_str_rt := c_recheck_dates_rec.id_str_rt;
            L_dc_dy_bsn := c_recheck_dates_rec.dc_dy_bsn;
            L_trx_BO_Count := c_recheck_dates_rec.BO_TRX_COUNT;
            L_trx_CO_Count := c_recheck_dates_rec.CO_TRX_COUNT;
            L_cpt_trx_read := 0;
            L_cpt_trx_upd := 0;
            L_LogMissingTrxFound := 'Y';
            
            L_LogLine :='Execution ID: ' || L_ExecID ||' Business Date: ' || L_dc_dy_bsn || ' BO Count: '||to_char(L_trx_BO_Count)||' - CO Count: ' ||to_char(L_trx_CO_Count);
            L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
            dbms_output.put_line (L_LogLine);


        FOR c_get_trx_rec IN c_get_trx
        LOOP
           L_rowid          := c_get_trx_rec.row_id;
           L_id_ws          := c_get_trx_rec.id_ws;
           L_ai_trn         := c_get_trx_rec.ai_trn;
           L_cpt_trx_read   := L_cpt_trx_read + 1; 
--            dbms_output.put_line (ROWIDTOCHAR(L_rowid));
           UPDATE orbo_owner.tr_trn 
              SET id_tlog_btch = -1
            WHERE rowid = L_rowid;

/*             
             
           UPDATE rco_schema.ct_trn_rpl_skips@CORPL 
              SET fl_cnt_rtry_tlog = fl_cnt_rtry_tlog + 1
            WHERE id_str_rt = L_id_str_rt
              AND id_ws = L_id_ws
              AND dc_dy_bsn = L_dc_dy_bsn
              AND ai_trn = L_ai_trn;
 
 IF SQL%ROWCOUNT = 0 THEN
              INSERT INTO rco_schema.ct_trn_rpl_skips@CORPL (id_str_rt, 
                                                          id_ws,
                                                          dc_dy_bsn,
                                                          ai_trn,
                                                          fl_tlog_ok,
                                                          fl_cnt_rtry_tlog,
                                                          ts_crt_rcrd,
                                                          ts_mdf_rcrd)
                                                  VALUES (L_id_str_rt,
                                                          L_id_ws,
                                                          L_dc_dy_bsn,
                                                          L_ai_trn,
                                                          1,
                                                          1,
                                                          SYSDATE,
                                                          SYSDATE);

           END IF;                                                        
*/              
           L_cpt_trx_upd := L_cpt_trx_upd + SQL%ROWCOUNT;

         COMMIT;
        END LOOP;

        L_LogLine :='Execution ID: ' || L_ExecID ||' Nb of trx missing: '||L_cpt_trx_read;
        L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
        dbms_output.put_line (L_LogLine);

        L_LogLine :='Execution ID: ' || L_ExecID ||' Nb of trx re-flagged for TLog replication : '||L_cpt_trx_upd;
        L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
        dbms_output.put_line (L_LogLine);


    END LOOP;
    
        IF L_cpt_trx_read = 0 THEN
            L_LogLine :='Execution ID: ' || L_ExecID || ' Store: '||L_store||' No missing trx found';
            L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
            dbms_output.put_line (L_LogLine);
        
        END IF;

        L_LogLine :='Execution ID: ' || L_ExecID || ' Store: '||L_store||' Execution end time : '||to_char(SYSDATE, 'YYYY-MM-DD hh24:mi:ss');
        L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
        --INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG) VALUES (L_ExecID, L_store, L_ReplType, L_LogLine );
        dbms_output.put_line (L_LogLine);


     INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG, TRX_MISSING) VALUES (L_ExecID, L_store, L_ReplType, L_LogTxt, L_LogMissingTrxFound );
     COMMIT;
     
     EXCEPTION
          when OTHERS then
             ROLLBACK;
             L_error_code := to_char(SQLCODE);
             L_error_message := 'PACKAGE_ERROR : '||SQLERRM||','||L_program||','||TO_CHAR(SQLCODE);
             L_LogLine :='Execution ID: ' || L_ExecID ||' Exception '||TO_CHAR(SQLCODE)||'~~'||L_error_message||', L_id_str_rt : '||L_id_str_rt||', L_id_ws : '||L_id_ws||', L_dc_dy_bsn : '||L_dc_dy_bsn||', L_ai_trn : '||L_ai_trn;
             L_LogTxt := L_LogTxt|| L_LogLine ||chr(13) ||  chr(10);
             INSERT INTO CT_TR_RPL_LOG@CORPL(EXECUTION_ID, STORE_ID, REPL_TYPE, LOG_MSG, TRX_MISSING) VALUES (L_ExecID, L_store, L_ReplType, L_LogTxt, L_LogMissingTrxFound );
             COMMIT;
                dbms_output.put_line (L_LogLine);
                
END;
/
COMMIT ;
/
DROP DATABASE LINK "CORPL";
/
spool off    
EXIT;



