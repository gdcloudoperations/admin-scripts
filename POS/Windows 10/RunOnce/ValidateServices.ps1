$object = gwmi win32_operatingsystem | select csname, @{LABEL='LastBootUpTime';EXPRESSION={$_.ConverttoDateTime($_.lastbootuptime)}} 
$pw = convertto-securestring "S3cure!!733" -asplaintext �force
$pp = new-object -typename System.Management.Automation.PSCredential -argumentlist ".\pos0",$pw

if((Get-Date).AddMinutes(-5) -lt $object.LastBootUpTime -and -Not(Test-Path("C:\OracleRetailStore\Client\pos"))) {
    #$ErrorActionPreference = "SilentlyContinue" #Remove Errors during process
    Write-Output "Running Services / Demarrage des Services"
    $weblogicService = Get-Service | Where-Object { $_.Name -like 'beasvc *' }
    $oracleService =  Get-Service -Name 'OracleOraDb11g_home1TNSListener'
    $StoreService = Get-Service -Name 'StoreServer'

    Start-Process cmd -Credential $pp -ArgumentList "/c net stop $($oracleService.Name) /y"
    Start-Process cmd -Credential $pp -ArgumentList "/c net stop `"$($weblogicService.Name)`" /y"
    Start-Process cmd -Credential $pp -ArgumentList "/c net stop $($StoreService.Name) /y"
    Start-Sleep 30
    while(-NOT($($(Test-NetConnection -Port 1521 -computerName localhost).TcpTestSucceeded))) {
        if($oracleService.Status -eq 'Stopped') {
            Start-Process cmd -Credential $pp -ArgumentList "/c net start $($oracleService.Name)"
        }
        $oracleService =  Get-Service -Name 'OracleOraDb11g_home1TNSListener'
    }
    while(-NOT($($(Test-NetConnection -Port 7001 -computerName localhost).TcpTestSucceeded)) -and -NOT($($(Test-NetConnection -Port 7002 -computerName localhost).TcpTestSucceeded))) {
        if($weblogicService.Status -eq 'Stopped') {
            Start-Process cmd -Credential $pp -ArgumentList "/c net start `"$($weblogicService.Name)`" /y"
        }
        $weblogicService = Get-Service | Where-Object { $_.Name -like 'beasvc *' }
    }
    Start-Process cmd -Credential $pp -ArgumentList "/c net start $($StoreService.Name) /y"
}

#if(Test-Path("C:\OracleRetailStore\Client\pos")) {
#    $printers = get-wmiobject -class win32_printer -computer storeserver -Credential $pp | Select * | Where-Object { ($_.Name -like "B$($env:STORE_NUMBER)" -or $_.Name -like "D$($env:STORE_NUMBER)") -and ($_.ShareName -ne "")}
#    $printers | foreach-object { 
#        if((Get-Printer "\\storeserver\$($_.ShareName)" -ErrorAction SilentlyContinue).Count -eq 0) {
#           printui /q /in /n\\storeserver\$($_.ShareName)
#        }
#    }
#}
