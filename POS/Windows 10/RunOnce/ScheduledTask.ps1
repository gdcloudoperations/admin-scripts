﻿param (
    [Parameter(Mandatory = $true)]
    [string]$username,
    [Parameter(Mandatory = $true)]
    [string]$password
)

function Set-Recovery{
    param
    (
        [string] [Parameter(Mandatory=$true)] $ServiceDisplayName,
        [string] [Parameter(Mandatory=$true)] $Server,
        [string] $action1 = "restart",
        [int] $time1 =  30000, # in miliseconds
        [string] $action2 = "restart",
        [int] $time2 =  30000, # in miliseconds
        [string] $actionLast = "restart",
        [int] $timeLast = 30000, # in miliseconds
        [int] $resetCounter = 4000 # in seconds
    )
    $serverPath = "\\" + $server
    $services = Get-CimInstance -ClassName 'Win32_Service' | Where-Object {$_.DisplayName -imatch $ServiceDisplayName}
    $action = $action1+"/"+$time1+"/"+$action2+"/"+$time2+"/"+$actionLast+"/"+$timeLast

    foreach ($service in $services){
        # https://technet.microsoft.com/en-us/library/cc742019.aspx
        $output = sc.exe $serverPath failure $($service.Name) actions= $action reset= $resetCounter
    }
}

#SET Java Path
[System.Environment]::SetEnvironmentVariable("PATH", $Env:Path + "$($env:JAVA_HOME)\bin")

#Task schedule for Back Office
if(-Not(Test-Path("C:\OracleRetailStore\Client\pos"))) {
    #DB Backup
    $action = New-ScheduledTaskAction -Execute 'C:\FromHQ\db\dbbackup.bat'
    $trigger =  New-ScheduledTaskTrigger -Daily -At 4am
    $timeSpan = New-TimeSpan -Hours 2
    $settingsSet = New-ScheduledTaskSettingsSet -ExecutionTimeLimit $timeSpan -Compatibility Win7 -WakeToRun
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "dbbackup" -User $username -Password $password -RunLevel Highest -Settings $settingsSet -Force

    #RMAN Backup
    $action = New-ScheduledTaskAction -Execute 'C:\FromHQ\Backoffice\db_backup\rman_backup.bat'
    $trigger =  New-ScheduledTaskTrigger -Daily -At 6am
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "DB_rman_backup" -User "System" -RunLevel Highest -Force
    
    #DB Folder Cleanup
    $action = New-ScheduledTaskAction -Execute 'C:\FromHQ\Backoffice\db_backup\DelEmpty.bat'
    $trigger =  New-ScheduledTaskTrigger -Daily -At 6am
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "DB_folder_cleanup" -User "System" -RunLevel Highest -Force

    #Clean up logs WL
    $action = New-ScheduledTaskAction -Execute 'C:\FromHQ\Backoffice\db_backup\menage_WL.vbs'
    $trigger =  New-ScheduledTaskTrigger -Weekly -At 6am -DaysOfWeek Tuesday
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "clean_up_logs_wl" -User "System" -RunLevel Highest -Force

    #Clean up logs DB
    $action = New-ScheduledTaskAction -Execute 'C:\FromHQ\Backoffice\db_backup\menage_DB.vbs'
    $trigger =  New-ScheduledTaskTrigger -Weekly -At 6am -DaysOfWeek Tuesday
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "clean_up_logs_DB" -User "System" -RunLevel Highest -Force

    #Clean db Backups
    $action = New-ScheduledTaskAction -Execute 'Powershell.exe -NoProfile -ExecutionPolicy ByPass -command "Get-ChildItem -Force -Path C:\dbbackup | Where-Object {$_.CreationTime -gt (Get-Date).Date.AddDays(-180) } | Remove-Item"'
    $trigger =  New-ScheduledTaskTrigger -Daily -At 5am
    $timeSpan = New-TimeSpan -Hours 2
    $settingsSet = New-ScheduledTaskSettingsSet -ExecutionTimeLimit $timeSpan -Compatibility Win7 -WakeToRun
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "clean_up_db_files" -User $username -Password $password -RunLevel Highest -Settings $settingsSet -Force

    #Clean archives
    $action = New-ScheduledTaskAction -Execute 'Powershell.exe -NoProfile -ExecutionPolicy ByPass -command "Get-ChildItem -Force -Path c:\dataimport\archive | Where-Object {$_.CreationTime -gt (Get-Date).Date.AddDays(-14) } | Remove-Item"'
    $trigger =  New-ScheduledTaskTrigger -Daily -At 5am
    $timeSpan = New-TimeSpan -Hours 2
    $settingsSet = New-ScheduledTaskSettingsSet -ExecutionTimeLimit $timeSpan -Compatibility Win7 -WakeToRun
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "clean_up_archive_files" -User $username -Password $password -RunLevel Highest -Settings $settingsSet -Force

    #Fix Services as timeout and shutdown was wrongly set on Windows 7
    #Setting the timeout to 30 secs for run (AppThrottle) and for shutdown (AppStop*)
    cmd /c "$($env:ProgramData)\gpopos1\nssm.exe set StoreServer AppThrottle 5000"
    cmd /c "$($env:ProgramData)\gpopos1\nssm.exe set StoreServer AppStopMethodThreads 5000"
    cmd /c "$($env:ProgramData)\gpopos1\nssm.exe set StoreServer AppStopMethodConsole 5000"
    cmd /c "$($env:ProgramData)\gpopos1\nssm.exe set StoreServer AppStopMethodThreads 5000"
    cmd /c "$($env:ProgramData)\gpopos1\nssm.exe set StoreServer AppStopMethodWindow 5000"

    #Creating the correct service for WebLogic
    #Setting StoreServer Service to be dependent on the new service.
    #Remove the old WebLogicDomain service
    Remove-Item -Path c:\FromHQ\runOnce\createService.cmd -Force -ErrorAction SilentlyContinue
    New-Item -ItemType file c:\FromHQ\runOnce\createService.cmd -Force
    Add-Content c:\FromHQ\runOnce\createService.cmd '@echo off'
    Add-Content c:\FromHQ\runOnce\createService.cmd '%programdata%\gpopos1\nssm.exe shutdown "WebLogicDomain"'
    Add-Content c:\FromHQ\runOnce\createService.cmd '%programdata%\gpopos1\nssm.exe remove "WebLogicDomain" confirm'
    Add-Content c:\FromHQ\runOnce\createService.cmd '%programdata%\gpopos1\nssm.exe unset "StoreServer" dependonService'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set DOMAIN_NAME=DOMAIN'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set SERVER_NAME=Admin_00%STORE_NUMBER%'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set DOMAIN_HOME=C:\Oracle\Middleware\user_projects\domains\Domain_00%STORE_NUMBER%'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set USER_MEM_ARGS=-Xms256m -Xmx2048m -XX:CompileThreshold=8000 -XX:PermSize=128m  -XX:MaxPermSize=256m'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set HOST=localhost'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set PORT=7001'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set USERDOMAIN_HOME=%DOMAIN_HOME%'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set MAX_CONNECT_RETRIES=10'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'set WL_HOME=C:\Oracle\Middleware\wlserver_10.3'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'call "%DOMAIN_HOME%\bin\setDomainEnv.cmd" %*'
    Add-Content c:\FromHQ\runOnce\createService.cmd 'call "%WL_HOME%\server\bin\installSvc.cmd" %*'
    cmd /c "c:\FromHQ\runOnce\createService.cmd"

    $weblogicService = Get-Service | Where-Object { $_.Name -like 'beasvc *' }
    $oracleService =  Get-Service -Name 'OracleOraDb11g_home1TNSListener'
    $StoreService = Get-Service -Name 'StoreServer'
    cmd /c "sc config `"$($weblogicService.Name)`" depend=OracleOraDb11g_home1TNSListener"
    cmd /c "sc config `"$($StoreService.Name)`" depend=OracleOraDb11g_home1TNSListener"
    cmd /c "sc config `"$($StoreService.Name)`" depend=`"$($weblogicService.Name)`""
    Set-Recovery -ServiceDisplayName "$($oracleService.Name)" -Server "localhost"
    Set-Recovery -ServiceDisplayName "$($weblogicService.Name)" -Server "localhost"
    Set-Recovery -ServiceDisplayName "$($StoreService.Name)" -Server "localhost"
}