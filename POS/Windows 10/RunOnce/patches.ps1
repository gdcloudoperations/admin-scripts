#Run Q1-2019 patches
. ".\Q1-2019.ps1"

#Orbo reflag change
$file = "orboreflagtlogmissingtrx.sql"
$initialPath = ".\PATCHES\OrboReflag"
$destination = "C:\FromHQ\sql"
if(-NOT(Test-Path -Path $destination)) {
    New-Item -Path $destination -ItemType Directory -force
}
Copy-item -Path "$initialPath\$file" -Destination "$destination\$file" -Force

#POS-563
#. ".\POS-563.ps1"