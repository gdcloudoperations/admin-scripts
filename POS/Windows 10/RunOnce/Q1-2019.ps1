#credentials
$username=""
if(Test-path -Path "C:\users\pos0") {
    $username="pos0"
}
elseif(Test-path -Path "C:\users\Administrator") {
    $username="Administrator"
}
else {
    $username="Administrateur"
}
$password="S3cure!!733"

$startupPath = "c:\fromhq\runonce\patches\q1-2019"

if($env:STORE_CASH -ne "000")
{
    #BOTH
    #PIN-194
    $destination = 'C:\fipayeps\'
    New-item -Path $destination -ItemType Directory -Force
    if($env:STORE_COUNTRY -eq "CA"){
        #PIN-192 included
        Get-ChildItem -Path "$($startupPath)\PIN-194\CA_fipayeps_config.xml" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\fipayeps_config.xml" -force }
    }
    else {
        Get-ChildItem -Path "$($startupPath)\PIN-194\US_fipayeps_config.xml" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\fipayeps_config.xml" -force }
    }
    

    ##CA ONLY
    if($($env:STORE_COUNTRY) -eq "CA") {
        #PIN-191
        $destination = 'C:\fipayeps\'
        New-item -Path $destination -ItemType Directory -Force
        Get-ChildItem -Path "$($startupPath)\PIN-191\*" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\$($_.Name)" -force }

        #PIN-193
        $destination = 'C:\fipayeps\'
        New-item -Path $destination -ItemType Directory -Force
        Get-ChildItem -Path "$($startupPath)\PIN-193\*" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\$($_.Name)" -force }
    }

    #PIN-196
    $destination = 'C:\fipayeps\'
    Start-Process cmd -ArgumentList "/c net stop fipayeps /y"
    Start-sleep -Seconds 20
    New-item -Path $destination -ItemType Directory -Force
    Get-ChildItem -Path "$($startupPath)\PIN-196\*" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\$($_.Name)" -force }
    Get-ChildItem -Path "$($startupPath)\PIN-196\wanline\*" -Recurse | foreach-object { 
        if($_.FullName -like '*CA*' -and $env:STORE_COUNTRY -eq 'CA') {
            Copy-Item -Path $_.FullName -Destination "$($destination)\wanline.cfg" -force 
        }
        if($_.FullName -like '*US*' -and $env:STORE_COUNTRY -eq 'US') {
            Copy-Item -Path $_.FullName -Destination "$($destination)\wanline.cfg" -force 
        }
    }
    Start-Process cmd -ArgumentList "/c net start fipayeps /y"

    if($env:STORE_COUNTRY -eq "CA"){
        
    }

    #POS-553
    $destination = 'C:\ORACLERETAILSTORE\CLIENT\POS\PATCHES'
    New-item -Path $destination -ItemType Directory -Force
    Get-ChildItem -Path "$($startupPath)\POS-553\*" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\$($_.Name)" -force }

}
#else {
#    $destination = 'C:\FromHQ\Backoffice\dimp_Validation'
#    New-item -Path '$destination' -ItemType Directory -Force
#    Get-ChildItem -Path "$($startupPath)\POS-554\*" -Recurse | foreach-object { Copy-Item -Path $_.FullName -Destination "$($destination)\$($_.Name)" -force }
#
#    #Dimp Validation
#    $action = New-ScheduledTaskAction -Execute 'java -jar DIMPValidation.jar' -WorkingDirectory $destination
#    $trigger =  New-ScheduledTaskTrigger -Daily -At 5am
#    $timeSpan = New-TimeSpan -Hours 2
#    $settingsSet = New-ScheduledTaskSettingsSet -ExecutionTimeLimit $timeSpan -Compatibility Win7 -WakeToRun -MultipleInstances IgnoreNew
#    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "dimp_validation" -User $username -Password $password -RunLevel Highest -Settings $settingsSet -Force
#}

FUNCTION CheckAndCreate($varName, $value) {
    start-process cmd -ArgumentList "/c setx -m $($varName) $($value)"
}

CheckAndCreate -varName "STORE_LASTPATCH" -value "Q1-2019"