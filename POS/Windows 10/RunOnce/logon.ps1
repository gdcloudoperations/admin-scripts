﻿$GDI = "$($env:HOMEDRIVE)\ProgramData\Microsoft\Windows\Start Menu\GDI"
$DestinationPathDesktop = "$($env:HOMEPATH)\Desktop\"
$DestinationPathAllApps = "$($env:APPDATA)\Microsoft\Windows\Start Menu\Programs\"
$SourcePOS = "C:\OracleRetailStore\Client\pos\bin\ClientConduit.bat"
$WorkingDirectory = "C:\OracleRetailStore\Client\pos\bin\"
$POSIconLocation = "c:\fromhq\posclient\pos.ico"

$TSDiscon = "$($env:SystemRoot)\System32\tsdiscon.exe"
$WorkingDirectorySystem32 = "$($env:SystemRoot)\System32\"

$OpenOfficePath = "C:\Program Files (x86)\OpenOffice 4\program\"
$OpenOfficeWriter = "swriter.exe"
$OpenOfficeCalc = "scalc.exe"

Function CreateShortcut() {
    param ([String]$target, [String]$WorkingDirectory, [String]$icon, [String]$destination, [String]$arguments)
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$($destination)")
    $Shortcut.workingdirectory = "$($WorkingDirectory)"
    $Shortcut.IconLocation = "$($icon)"
    $Shortcut.TargetPath = "$($target)"
    $Shortcut.Arguments = "$($arguments)"
    $Shortcut.Save()
}

if(-Not(Test-Path("C:\OracleRetailStore\Client\pos")) -and $env:USERNAME -like "POS0") {
    #Switch User shortcut
    CreateShortcut -target "$($TSDiscon)" -WorkingDirectory "$($WorkingDirectorySystem32)" -icon "$($env:SystemRoot)\system32\imageres.dll,194" -destination "$($DestinationPathDesktop)TSDISCON.lnk"
}

if($env:USERNAME -like "POS1") {
    #Clean Account
    remove-item -path "$($env:APPDATA)\Microsoft\Windows\Start Menu\Programs\*" -recurse -force
    Remove-item -Path "$($env:LOCALAPPDATA)\Microsoft\Windows\WinX\*" -Recurse -force
    Remove-item -Path "$($env:HOMEPATH)\Desktop\*" -Recurse -force

    #Copy items over
    Copy-Item -Path "$($GDI)\*" -Recurse -Destination $DestinationPathDesktop
    Copy-Item -Path "$($GDI)\*" -Recurse -Destination $DestinationPathAllApps

    if(Test-Path("C:\OracleRetailStore\Client\pos")) {
        #Create Store POS Shortcut
        CreateShortcut -destination "$($DestinationPathDesktop)POS.lnk" -target "$($SourcePOS)" -WorkingDirectory "$($WorkingDirectory)" -icon "$($POSIconLocation)" 
        CreateShortcut -destination "$($DestinationPathAllApps)POS.lnk" -target "$($SourcePOS)" -WorkingDirectory "$($WorkingDirectory)" -icon "$($POSIconLocation)" 
        CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\inmoment.ico" -destination "$($DestinationPathDesktop)\InMoment.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://www.inmoment.com/report/app?service=page&page=Login"
        
        # Build and assemble URL elements for WebClock 
        $ClockMAC="GD_Store" + $env:STORE_NUMBER
        $ClockID='123$Store_' + $env:STORE_NUMBER
        $Enviro="dynamite"
        $Site="https://dynamite.dayforce.com"
        $DayforceWCURL=$Site+"/clock/index.html?cn="+$ClockMAC+ "&ns=" + $Enviro + "&sc=" +
        [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($ClockID))

        CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\WebClock.ico" -destination "$($DestinationPathDesktop)\Dayforce Clock.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments $DayforceWCURL

        start-process -WorkingDirectory "C:\OracleRetailStore\Client\pos\bin\" "C:\OracleRetailStore\Client\pos\bin\ClientConduit.bat"
    }
}

#OpenOffice for all accounts and all versions of POS (fo and bo)
CreateShortcut -destination "$($DestinationPathDesktop)Writer.lnk" -target "$($OpenOfficePath)$($OpenOfficeWriter)" -WorkingDirectory "$($OpenOfficePath)" -icon "$($OpenOfficePath)$($OpenOfficeWriter),0" 
CreateShortcut -destination "$($DestinationPathAllApps)Writer.lnk" -target "$($OpenOfficePath)$($OpenOfficeWriter)" -WorkingDirectory "$($OpenOfficePath)" -icon "$($OpenOfficePath)$($OpenOfficeWriter),0"
CreateShortcut -destination "$($DestinationPathDesktop)Spreadsheet.lnk" -target "$($OpenOfficePath)$($OpenOfficeCalc)" -WorkingDirectory "$($OpenOfficePath)" -icon "$($OpenOfficePath)$($OpenOfficeCalc),0" 
CreateShortcut -destination "$($DestinationPathAllApps)Spreadsheet.lnk" -target "$($OpenOfficePath)$($OpenOfficeCalc)" -WorkingDirectory "$($OpenOfficePath)" -icon "$($OpenOfficePath)$($OpenOfficeCalc),0"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\activeomni.ico" -destination "$($DestinationPathDesktop)\Active Omni.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://store-gdynp.omni.manh.com/"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\cognos.ico" -destination "$($DestinationPathDesktop)\Cognos.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://gdynp-sci.omni.manh.com/bi/"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\activeomni.ico" -destination "$($DestinationPathDesktop)\Change Omni Password.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://ft.dynamite.ca/adfs/portal/updatepassword/"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\OracleApp.ico" -destination "$($DestinationPathDesktop)\Sim Reports.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "http://sastoh01.corp.gdglobal.ca:9704/xmlpserver/Guest/SIM13/StoreRptStrtPage/StoreRptStrtPage.xdo?_xpf=&_xmode=4&_xf=html"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe,0" -destination "$($DestinationPathDesktop)\Store Console.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://storeconsole-prod.api.dynamite.ca/"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\WebClock.ico" -destination "$($_.ProfileImagePath)\Desktop\Dayforce.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://dynamite.dayforce.com/"
CreateShortcut -target "$(${env:ProgramFiles(x86)})\Google\Chrome\Application\chrome.exe" -icon "$($env:HOMEDRIVE)\ProgramData\gpopos1\ico\Corp.ico" -destination "$($_.ProfileImagePath)\Desktop\Prodco.lnk" -WorkingDirectory "$($WorkingDirectorySystem32)" -arguments "https://prostore.prodcotech.com/?client=dyn"
