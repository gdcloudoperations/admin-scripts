@echo off
REM Import the registry for all users. Depending on user, the logon will be different.
echo Loading the Hive
reg load "hku\Default" "C:\users\default\NTUSER.DAT"
IF %ERRORLEVEL% NEQ 0 echo FAILED: reg load "hku\Default" "C:\users\default\NTUSER.DAT" >> C:\FromHQ\RunOnce\_DONE.txt
echo Importing the reg
reg import C:\FromHQ\RunOnce\runonce.reg
IF %ERRORLEVEL% NEQ 0 echo FAILED: reg import runonce.reg >> C:\FromHQ\RunOnce\_DONE.txt
echo Closing the Hive
reg unload "hku\default"
IF %ERRORLEVEL% NEQ 0 echo FAILED: reg unload "hku\default" >> C:\FromHQ\RunOnce\_DONE.txt

REM Administrator disablement.
echo Disable Administrator
IF %STORE_LANGUAGE% EQU EN net user Administrator /active:no
IF %STORE_LANGUAGE% EQU FR net user Administrateur /active:no
IF %ERRORLEVEL% NEQ 0 echo FAILED: net user Administrat(or)(eur) /active:no >> C:\FromHQ\RunOnce\_DONE.txt

REM Enable Autologin for POS1 after full deployment
echo Enable Autologin
reg import C:\FromHQ\RunOnce\autologin.reg
IF %ERRORLEVEL% NEQ 0 echo FAILED: reg import autologin.reg >> C:\FromHQ\RunOnce\_DONE.txt

REM Disable the USB Selective Suspend for USB devices
echo USB Suspend
reg import C:\FromHQ\RunOnce\USBSelectiveSuspend.reg
IF %ERRORLEVEL% NEQ 0 echo FAILED: reg import C:\FromHQ\RunOnce\USBSelectiveSuspend.reg >> C:\FromHQ\RunOnce\_DONE.txt
Powercfg.exe -setacvalueindex 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c 2a737441-1930-4402-8d77-b2bebba308a3 48e6b7a6-50f5-4782-a5d4-53bb8f07e226 0
IF %ERRORLEVEL% NEQ 0 echo FAILED: Powercfg.exe -setacvalueindex 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c 2a737441-1930-4402-8d77-b2bebba308a3 48e6b7a6-50f5-4782-a5d4-53bb8f07e226 0 >> C:\FromHQ\RunOnce\_DONE.txt

REM Make sure the history of the for task scheduling is enabled
echo Task Scheduler history
wevtutil set-log Microsoft-Windows-TaskScheduler/Operational /enabled:true
IF %ERRORLEVEL% NEQ 0 echo FAILED: wevtutil set-log Microsoft-Windows-TaskScheduler/Operational /enabled:true >> C:\FromHQ\RunOnce\_DONE.txt

REM Create the schedules needed for BO and FO
echo Creating Task Schedule for DBBackups
Powershell.exe -NoProfile -ExecutionPolicy ByPass -file C:\\FromHQ\\RunOnce\\ScheduledTask.ps1 -username %1 -password %2
IF %ERRORLEVEL% NEQ 0 echo FAILED: Powershell.exe -NoProfile -ExecutionPolicy ByPass -file C:\\FromHQ\\RunOnce\\ScheduledTask.ps1 >> C:\FromHQ\RunOnce\_DONE.txt

REM Apply the security settings for BO and FO after finalizing the deployment.
echo Applying the security settings
secedit /configure /db %temp%\sec.sdb /cfg "C:\Windows\Temp\StoreWin10GPO\comp_security.inf"
IF %ERRORLEVEL% NEQ 0 echo FAILED: secedit /configure /db %temp%\sec.sdb /cfg "C:\Windows\Temp\StoreWin10GPO\comp_security.inf" >> C:\FromHQ\RunOnce\_DONE.txt

REM Set Accounts to Never Expire
echo POS0 and POS1 expiry never
WMIC USERACCOUNT WHERE Name='pos0' SET PasswordExpires=FALSE
IF %ERRORLEVEL% NEQ 0 echo FAILED: WMIC USERACCOUNT WHERE Name='pos0' SET PasswordExpires=FALSE >> C:\FromHQ\RunOnce\_DONE.txt
WMIC USERACCOUNT WHERE Name='pos1' SET PasswordExpires=FALSE
IF %ERRORLEVEL% NEQ 0 echo FAILED: WMIC USERACCOUNT WHERE Name='pos1' SET PasswordExpires=FALSE >> C:\FromHQ\RunOnce\_DONE.txt

REM Install patches
echo Installing Patches
Powershell.exe -NoProfile -ExecutionPolicy ByPass -file C:\\FromHQ\\RunOnce\\patches.ps1 -username %1 -password %2
IF %ERRORLEVEL% NEQ 0 echo FAILED: Powershell.exe -NoProfile -ExecutionPolicy ByPass -file C:\\FromHQ\\RunOnce\\patches.ps1 >> C:\FromHQ\RunOnce\_DONE.txt