Param (
    [String]$MinutesBack = 60,
    [String]$SpecificCN = "",
    [Boolean]$UpdateStores = $true,
    [Boolean]$AllUsers = $true,
    [Boolean]$AllTime = $false,
    [Boolean]$StoresOnly = $false,
    [Boolean]$CleanDisabled = $true,
    [Parameter(Mandatory=$true)][String]$LDAPPassword)

Function SearchInquiry() { #Search the LDAP
    Param (
    [Parameter(Mandatory=$True)]
    [String]$queryDN,
    [Parameter(Mandatory=$True)]
    [String]$queryFilter,
    [Parameter(Mandatory=$True)]
    [int]$resultSize)

    $useragent = 'cn=manager,dc=stores,dc=corp,dc=gdglobal,dc=ca'
    $userpass = $LDAPPassword
    $auth = [System.DirectoryServices.AuthenticationTypes]::FastBind

    $root = New-Object -TypeName System.DirectoryServices.DirectoryEntry($queryDN, $useragent, $userpass, $auth)
    $Searcher = New-Object System.DirectoryServices.DirectorySearcher($root,$queryFilter)
    $Searcher.Asynchronous = $true
    if($resultSize -eq 1) {
        return $Searcher.FindOne()
    }
    else {
        return ($Searcher.FindAll())
    }
}

Function getStoresFilter() { #Create filter for stores depending on parameters
    $filterStores = "(&(objectClass=simStore)"
    if(-not($AllTime)) {
        $filterStores = "$($filterStores)(modifyTimestamp>=$($timeframe))"
    }
    return "$($filterStores))"
}

Function getUsersFilter() { #Create filter for Users depending on parameters
    $filterUsers = "(&(objectClass=simUser)"
    if($SpecificCN -ne "") {
        $filterUsers = "$($filterUsers)(cn=$($specificCN))"
        return "$($filterUsers))"
    }
    if($AllUsers) {
        $filterUsers = "$($filterUsers)(empStatus=*)"
    }
    else {
        $filterUsers = "$($filterUsers)(|(empStatus=0)(empStatus=3))"
    }
    if(-not($AllTime)) {
        $filterUsers = "$($filterUsers)(modifyTimestamp>=$($timeframe))"
    }
    return "$($filterUsers))"
}

Function StoresCreation() { #Create Stores from LDAP. Not created the deleted and unused stores 
    $Stores = SearchInquiry -queryDN "$domain$simStoresRdn,$baseDN" -queryFilter $(getStoresFilter) -resultSize 999999
    #Start by creating the stores in AD
    $countFullStores = $Stores.count
    foreach ($s in $Stores) {
        $countStores = $countStores + 1
        $store = $s.GetDirectoryEntry()
        $storeId = $($store.StoreId.Value.Substring(2,3))
        $group = Get-ADObject -LDAPFilter "(&(objectClass=Group)(Name=*$storeId))" -SearchBase "OU=STORES,OU=GROUPS,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca"
        $percent = [math]::round((($countStores/$countFullStores)*100))
        Write-Progress "Creating $countStores of $($countFullStores)" -Status "Store: $storeId is being created" -PercentComplete $percent -Id 0
        if($group -eq $null) {
            $DN = Get-ADObject -LDAPFilter "(&(objectClass=Contact)(Name=*$storeId*))" -SearchBase "OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" -ResultSetSize 1 | SELECT @{l='Parent';e={([adsi]"LDAP://$($_.DistinguishedName)").Parent}}
            if($DN -ne $null) {
                $brand = $DN.Parent.Split(',')[0].Split('=')[1]
                if($brand -like 'GARAGE') { $storeName =  "GARAGE$($store.StoreId)" } else { $storeName = "DYNAMITE$($store.StoreId)" }
                New-ADGroup -Name $storeName -SamAccountName $storeName -GroupCategory Security -GroupScope Global `
                -DisplayName $storeName -Path "OU=STORES,OU=GROUPS,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" `
                -Description $storeName -PassThru | % {Add-ADGroupMember $simStores -Members $_}
            }
            else {
                #This is a closed store. Will not create
                <#$storeName = "STORE$($store.StoreId)"
                New-ADGroup -Name $storeName -SamAccountName $storeName -GroupCategory Security -GroupScope Global `
                -DisplayName $storeName -Path "OU=STORES,OU=GROUPS,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" `
                -Description $storeName -PassThru | % {Add-ADGroupMember $simStores -Members $_}#>
            }
        } 
    }
    Write-Progress "Creating $countStores of $($countFullStores)" -Id 0 -Completed
}

Function Set-OmniAccess() {
    Param(
        [Parameter(Mandatory=$True)]
        [Object]$User
    )
    $jc = $User.extensionAttribute7.Replace('hcmJobCode=','')
    $group = ""
    switch($jc) {
        'HO-SIM SU' { $group = 'OMNI_BUS_SUPER_USER' }
        'HO-IT-APPS-SUPPORT' { $group = 'OMNI_IT_CONFIG_ADMIN' }
        'SAM' { $group = 'OMNI_SAM' }
        'HO-Procurement' { $group = 'OMNI_Procurement' }
        'HO-INV-CTL' { $group = 'OMNI_INVENTORY_CONTROL' }
        'HO-LP' { $group = 'OMNI_LOSS_PREVENTION' }
        'Head Mer' { $group = 'OMNI_HM' }
        'Stock _clerk' { $group = 'OMNI_FRANCHISEE' }
        'DC-Corp' { $group = 'OMNI_LOGISTICS' }
        'DIR' { $group = 'OMNI_DIRECTOR' }
        'SUP' { $group = 'OMNI_SUPERVISOR' }
        {$_ -eq 'K' -or $_ -eq 'K PT'} { $group = 'OMNI_THIRD_KEY' }
        {$_ -eq 'A' -or $_ -eq 'ASSOC M' -or $_ -eq 'M' -or $_ -eq 'SR MER' -or $_ -eq 'TR ST MGR' -or $_ -eq 'VA'}  { $group = 'OMNI_MANAGEMENT' }
        {$_ -eq 'SUPPORT_ASSOCIATE PT' -or $_ -eq 'SUPPORT_ASSOCIATE FT' -or $_ -eq 'SUPPORT ASSOCIATE FT' -or $_ -eq 'SUPPORT ASSOCIATE PT'} { $group = 'OMNI_SUPPORT_ASSOCIATE' }
        {$_ -eq 'SEASONAL' -or $_ -eq 'PT' -or $_ -eq 'FT'} { $group = 'OMNI_SALES_ASSOC' }
        default { $group = 'OMNI_NOACCESS' }
    }
    Write-Progress "Providing Access" -Status "Adding access to $($group)" -PercentComplete 50 -ParentId 1 -id 4
    Add-ADGroupMember -Identity $group -Members $User.samAccountName
    Write-Progress "Providing Access" -Status "Adding access to $($group)" -PercentComplete 100 -ParentId 1 -id 4 -Completed
}

Function UsersCreation() {
    #Assign default store and add member to the other stores. Also set the defaultStore (extensionAttribute1), 
    #birthDate (extensionAttribute2), personalEmail (extensionAttribute3), personalPhone (extensionAttribute4), superUser (extensionAttribute5),
    #manager (extensionAttribute6), hcmJobCode (extensionAttribute7) attributes

    $Users = SearchInquiry -queryDN "$domain$userRdn,$baseDN" -queryFilter $(getUsersFilter) -resultSize 999999
    $countUser = 0
    $countFullUsers = $Users.count
    foreach ($u in $Users) {
        $countUser = $countUser + 1
        $r = $u.GetDirectoryEntry()

        $percent = [math]::round((($countUser/$countFullUsers)*100))
        Write-Progress "Creating $countUser of $countFullUsers" -Status "Creating $($r.uid.Value): $($r.givenName.Value) $($r.sn.Value)" -PercentComplete $percent -Id 1
        if($r.defaultStore -ne $null) { $defaultStore = "defaultStore=$($r.defaultStore.Value.Split(',')[0].Split('=')[1])" } else { $defaultStore = "defaultStore=00001" }
        if($r.birthDate -ne $null) { $birthDate = "birthDate=$($($r.birthDate).ToString('0000/00/00'))" } else { $birthDate = "birthDate=NA" }
        if($r.mail -ne $null) { $personalEmail = "personalEmail=$($r.mail)" } else { $personalEmail = "personalEmail=unknownrecipient@dynamite.ca" }
        if($r.mobile -ne $null) { $personalPhone = "personalPhone=$($r.mobile)" } else { $personalPhone = "personalPhone=NA" }
        if($r.superUser -ne $null) { $superUser = "superUser=$($r.superUser)" } else { $superUser = "superUser=FALSE" }
        if($r.manager -ne $null) { $manager = "manager=$($r.manager)" } else { $manager = "manager=NA" }
        if($r.hcmJobCode -ne $null) { $hcmJobCode = "hcmJobCode=$($r.hcmJobCode)" } else { $hcmJobCode = "hcmJobCode=NA" }
        if([int]$r.uid.Value -lt 1000) { $sam = "00$($r.uid.Value)" } elseif( [int]$r.uid.Value -lt 10000 ) { $sam = "0$($r.uid.Value)" } else { $sam = $r.uid.Value }
        $ADUser = $null
        try {
            $ADUser = GET-ADUSER -identity $sam -ErrorAction SilentlyContinue
        }
        catch {
            $ADUser = $null
        }
    
        if($ADUser -ne $null) {
            $currentUser = SET-ADUSER $ADUser -replace @{'extensionAttribute1' = $defaultStore; 'extensionAttribute2' = $birthDate;`
            'extensionAttribute3' = $personalEmail; 'extensionAttribute4' = $personalPhone; 'extensionAttribute5' = $superUser; `
            'extensionAttribute6' = $manager; 'extensionAttribute7' = $hcmJobCode} `
            -Enabled $(if(($r.empStatus.Value -eq 0) -or ($r.empStatus.Value -eq 3)) { $true } else { $false }) -PassThru
        }
        else {
            if($r.preferredCountry.Value -eq 'CA') { $pathOU = "OU=EMPLOYEES,OU=CANADA,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" } else { $pathOU = "OU=EMPLOYEES,OU=USA,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" }
            $initialPW = ConvertTo-SecureString -String "Dynamite123!" -AsPlainText -Force
            $currentUser = NEW-ADUSER -GivenName $r.givenName.Value -SamAccountName $sam -EmployeeID $sam `
            -Country $r.preferredCountry.Value -Name $sam -DisplayName "$($r.givenName.Value) $($r.sn.Value)" `
            -EmployeeNumber $sam -Enabled $(if(($r.empStatus.Value -eq 0) -or ($r.empStatus.Value -eq 3)) { $true } else { $false }) -Path $pathOU `
            -OtherAttributes @{'extensionAttribute1' = $defaultStore; 'extensionAttribute2' = $birthDate;`
            'extensionAttribute3' = $personalEmail; 'extensionAttribute4' = $personalPhone; 'extensionAttribute5' = $superUser; `
            'extensionAttribute6' = $manager; 'extensionAttribute7' = $hcmJobCode; 'cn' = $sam} `
            -Surname $r.sn.Value -PassThru -AccountPassword $initialPW -Description "$($r.givenName.Value) $($r.sn.Value)" -UserPrincipalName "$($sam)@corp.gdglobal.ca" -ChangePasswordAtLogon
        }
        if($currentUser.Enabled) {
            #Set other memberships in other stores
            $count = 0
            $countAllStores = $r.userStores.Count
            foreach($us in $r.userStores) {
                $count = $count + 1
                $currentStore = Get-ADObject -LDAPFilter "(&(objectClass=Group)(Name=*$($us.Split(',')[0].Split('=')[1])*))" -SearchBase "OU=STORES,OU=GROUPS,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca"
                $percent = [math]::round((($count/$countAllStores)*100))
                Write-Progress "Adding to stores: $count of $countAllStores" -Status "Adding to $($currentStore.Name)" -PercentComplete $percent -ParentId 1 -id 2
                if($currentStore -ne $null) {
                    if(-not($(Get-ADGroupMember -Identity $currentStore -Recursive | Select -ExpandProperty Name) -contains $currentUser.Name)) {
                        Add-ADGroupMember $currentStore -Members $currentUser -ErrorAction SilentlyContinue
                    }
                }
            }
            Write-Progress "Adding to stores: $count of $countAllStores" -Status "Adding to $($currentStore.Name)" -PercentComplete 100 -ParentId 1 -id 2 -Completed
            #Add to Role
            #Get user role
            $UserRole = SearchInquiry -queryDN "$($domain)cn=$($r.uid.Value),$userRdn,$baseDN" -queryFilter "(&(objectClass=simUserRole))" -resultSize 1
            $groupRole = Get-ADObject -LDAPFilter "(&(objectClass=Group)(Name=SIM_$($(if($UserRole.Count -eq 0){ "NOACCESS" } else { $(if($UserRole.GetDirectoryEntry()[0].roleName -eq "SALES_ASSOCIATE") { "SALES_ASSOCIATE" } else { $UserRole.GetDirectoryEntry()[0].roleName }) }))))" -SearchBase "OU=ACCESS,OU=GROUPS,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" -ResultSetSize 1
            try {
                Write-Progress "Providing Access" -Status "Adding access to $($groupRole.Name)" -PercentComplete 50 -ParentId 1 -id 3
                Get-ADuser $currentUser.SamAccountName | Remove-ADPrincipalGroupMembership -MemberOf $(Get-ADPrincipalGroupMembership $currentUser.SamAccountName | Where-Object { $_.Name -like "SIM_*" }) -Confirm:$false -ErrorAction SilentlyContinue
                Get-ADuser $currentUser.SamAccountName | Remove-ADPrincipalGroupMembership -MemberOf $(Get-ADPrincipalGroupMembership $currentUser.SamAccountName | Where-Object { $_.Name -like "OMNI_*" }) -Confirm:$false -ErrorAction SilentlyContinue
            }
            catch {}
            finally {
                Add-ADGroupMember $groupRole -Members $currentUser -ErrorAction SilentlyContinue
                Set-OmniAccess -User (Get-ADUser $currentUser.samAccountName -Properties extensionAttribute7, samAccountName)
                Write-Progress "Providing Access" -Status "Adding access to $($groupRole.Name)" -PercentComplete 100 -ParentId 1 -id 3 -Completed
            }
        }
        else {
            Get-ADuser $currentUser.SamAccountName | Remove-ADPrincipalGroupMembership -MemberOf $(Get-ADPrincipalGroupMembership $currentUser.SamAccountName | Where { $._Name -not -like "Domain*"}) -Confirm:$false -ErrorAction SilentlyContinue
        }
        
    } 
}

Function CleanUsersAD() { #Clean AD from Disabled for more than 15 days
    Search-ADAccount -AccountDisabled -UsersOnly -SearchBase "OU=EMPLOYEES,OU=CANADA,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" `
    | Where-Object { $_.lastlogondate -ge $($(Get-Date).AddDays(-15).ToShortDateString()) } | Remove-ADUser -Confirm:$false
    Search-ADAccount -AccountDisabled -UsersOnly -SearchBase "OU=EMPLOYEES,OU=USA,OU=GROUP STORES,OU=USERS ACCOUNTS,DC=corp,DC=gdglobal,DC=ca" `
    | Where-Object { $_.lastlogondate -ge $($(Get-Date).AddDays(-15).ToShortDateString()) } | Remove-ADUser -Confirm:$false
}
#Main
$ErrorActionPreference = "SilentlyContinue" #Remove collection errors because of LDAP connection
#variables needed
$storeId = $null
$currentUser = $null
$currentStore = $null
$simStores = GET-ADGROUP "SIM_STORES"
$birthDate = $null
$defaultStore = $null
$countStores = 0
$baseDN = 'dc=stores,dc=corp,dc=gdglobal,dc=ca'
$userRdn="cn=Users"
$simRdn="cn=SIM"
$simStoresRdn="cn=SIMStores,cn=SIM"
$simRolesRdn="cn=SIMRoles,cn=SIM"
$domain = "LDAP://saldph01.corp.gdglobal.ca:389/"

#Get date 15 minutes ago
$timeNowMinus15 = $(Get-date).AddMinutes(-$MinutesBack)
$timeframe = "$($($($timeNowMinus15).Year).ToString("00"))$($($($timeNowMinus15).Month).ToString("00"))$($($($timeNowMinus15).day).ToString("00"))$($($($timeNowMinus15).Hour).ToString("00"))$($($($timeNowMinus15).Minute).ToString("00"))$($($($timeNowMinus15).Second).ToString("00"))Z"


if($UpdateStores) {
    StoresCreation
}

if(-not($StoresOnly)) {
    UsersCreation
}

if($CleanDisabled) {
    CleanUsersAD
}