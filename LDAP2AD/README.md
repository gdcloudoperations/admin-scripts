This is a powershell script to get the users created under the open ldap SALDPH01 and insert them in AD 

### Paramaters:
[String]$MinutesBack = 15,
[String]$SpecificCN = "",
[Boolean]$UpdateStores = $true,
[Boolean]$AllUsers = $false,
[Boolean]$AllTime = $false,
[Boolean]$StoresOnly = $false,
[Boolean]$CleanDisabled = $true,
[Parameter(Mandatory=$true)][String]$LDAPPassword)

MinutesBack - [Optional] Number of minutes to go back in time to search from users. Not needed if AllTime is set to true. Defaulted to 15 minutes. Value in minutes
SpecificCN - [Optional] If mentioned, MinutesBack/AllUsers/AllTime are completely ignored. Used to import a specific user from open ldap for update or insert
UpdateStores - [Optional] If set to false, will not include the stores in the insert/update script. Default is true
AllUsers - [Optional] If set to true, will look at all users (disabled or not) in the openldap. By default, only active users are looked into
AllTime - [Optional] If set to true, MinutesBack is ignored. Will check all users regardless of its modified time. Default is false
StoresOnly - [Optional] If set to true, only stores are updated. MinutesBack is considered by default. AllTime could be necessary if looking for all stores. Default is false
CleanDisabled - [Optional] If set to false, will not clean the Active Directory from users disabled for more than 15 days. Default is true
LDAPPassword - [Required] Password needed for cn=manager for Open Ldap.

#### Default behavior:
Users and stores uodated in the last 15 minuted will be inserted/updated in AD. Only users disabled for 15 days will be deleted from AD.

##### Example use:
LDAP2AD.ps1 -AllUsers $true -LDAPPassword <password>
Will insert/update stores in the last minutes and will insert/update users in the last 15 minutes. If a user is disbaled in LDAP, it will disable it in AD. Will also remove the users from AD that have a modified date > 15 days.

LDAP2AD.ps1 -AllUsers $true -AllTime $true -LDAPPassword <password>
Same behavior as previous but without considering time except for the removing of users (always set to 15 days)

LDAP2AD.ps1 -StoresOnly $true -AllTime $true -CleanDisabled $false -LDAPPassword <password>
Will only insert/update stores without considering time. Will not clean the AD

LDAP2AD.ps1 -UpdateStores $false -MinutesBack 1440 -CleanDisabled $false -LDAPPassword <password>
Will only insert/update users in the last day (1440 minutes). Will not clean the AD.

If you want to synchronize Open LDAP with AD, then: LDAP2AD.ps1 -AllUsers $true -AllTime $true -LDAPPassword <password> will need to be ran.

If a user is already in AD, then the script will update a set of fields in AD. These fields include:
* defaultStore (extensionAttribute1)
* birthDate (extensionAttribute2)
* personalEmail (extensionAttribute3)
* personalPhone (extensionAttribute4)
* superUser (extensionAttribute5)
* manager (extensionAttribute6)
* hcmJobCode (extensionAttribute7)
* enabled status